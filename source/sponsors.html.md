---
title: Sponsors
created_at: 2019-07-02
layout: sponsors
---

To support the development of Inko, we use [Open Collective][open-collective]
and [GitHub Sponsors][github]. Donations will be used to cover recurring costs
(such as hosting costs), and will one day be used to fund the development of
Inko.

If you want to support Inko's development, please consider donating using one of
the following services:

* [Open Collective][open-collective]
* [GitHub Sponsors][github]

Users and organisations who donate will be displayed on this page. Those who
donate at least €100/month will also have their logo and a link displayed on the
homepage.

The data on this page is refreshed periodically, so it may take several days for
your name to show up after donating for the first time.

## Recurring costs

Our recurring costs are as follows:

| What                         | Amount  | Interval
|------------------------------|---------|----------
| Website hosting at AWS       | €15 ±€5 | Monthly

## Budget

[open-collective]: https://opencollective.com/inko-lang
[github]: https://github.com/sponsors/YorickPeterse
