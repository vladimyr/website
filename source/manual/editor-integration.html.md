---
title: Editor integration
---

## Vim

We provide an official [Vim plugin for
Inko](https://gitlab.com/inko-lang/inko.vim). This plugin provides support for
syntax highlighting, indentation, folding, and more. Using
[vim-plug](https://github.com/junegunn/vim-plug) you can install it by adding
the following to your Vim configuration:

```vim
Plug 'https://gitlab.com/inko-lang/inko.vim.git'
```

Then restart Vim, and run `:PlugInstall` to install the plugin.
