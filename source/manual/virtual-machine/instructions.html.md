---
title: Instructions
---
<!-- vale off -->

## Table of contents
{:.no_toc}

1. TOC
{:toc}

## Introduction

IVM has quite a few instructions. Some of these instructions are rather
low-level, while others are high-level instructions such as `DirectoryList`.

The instruction set is a register based instruction set, based on
[Three-address code][tac].

## Available instructions

### ArrayAt

Gets the value of an array index.

This instruction requires three arguments:

1. The register to store the value in.
2. The register containing the array.
3. The register containing the index.

This instruction will set nil in the target register if the array index is out
of bounds. A negative index can be used to indicate a position from the end of
the array.

### ArrayClear

Removes all elements from an array.

This instruction requires one argument: the register of the array.

### ArrayLength

Gets the amount of elements in an array.

This instruction requires two arguments:

1. The register to store the length in.
2. The register containing the array.

### ArrayRemove

Removes a value from an array.

This instruction requires three arguments:

1. The register to store the removed value in.
2. The register containing the array to remove a value from.
3. The register containing the index.

This instruction sets nil in the target register if the index is out of bounds.
A negative index can be used to indicate a position from the end of the array.

### ArraySet

Inserts a value in an array.

This instruction requires four arguments:

1. The register to store the result (the inserted value) in.
2. The register containing the array to insert into.
3. The register containing the index (as an integer) to insert at.
4. The register containing the value to insert.

If an index is out of bounds the array is filled with nil values. A negative
index can be used to indicate a position from the end of the array.

### AttributeExists

Checks if an attribute exists in an object.

This instruction requires three arguments:

1. The register to store the result in (true or false).
2. The register containing the object to check.
3. The register containing the attribute name.

### BlockMetadata

Obtains metadata from a block.

This instruction requires three arguments:

1. The register to store the result in.
2. The register containing the block to obtain the data from.
3. The register containing an integer describing what kind of information to
   obtain.

The following kinds of metadata are available:

| Value | Meaning
|:------|:----------------------------------
| `0`   | The name of the block.
| `1`   | The file path of the block.
| `2`   | The line number of the block.
| `3`   | The argument names of the block.
| `4`   | The number of required arguments.
| `5`   | A boolean indicating if the last argument is a rest argument.

### ByteArrayAt

Returns the value at the given position in a byte array.

This instruction requires three arguments:

1. The register to store the value in.
2. The register containing the byte array to retrieve the value from.
3. The register containing the value index.

This instruction will set the target register to nil if no value was found.

### ByteArrayClear

Removes all elements from a byte array.

This instruction only requires one argument: the register containing the byte
array to clear.

### ByteArrayEquals

Checks two byte arrays for equality.

This instruction requires three arguments:

1. The register to store the result in as a boolean.
2. The register containing the byte array to compare.
3. The register containing the byte array to compare with.

### ByteArrayFromArray

Creates a new byte array from an array of integers.

This instruction requires two arguments:

1. The register to store the result in.
2. The register containing an array of integers to use for creating the byte
array.

This instruction will panic if any of the bytes is not in the range 0..256.

### ByteArrayLength

Gets the amount of elements in a byte array.

This instruction requires two arguments:

1. The register to store the length in.
2. The register containing the byte array.

### ByteArrayRemove

Removes a value from a byte array.

This instruction requires three arguments:

1. The register to store the removed value in.
2. The register containing the byte array to remove a value from.
3. The register containing the index of the value to remove.

This instruction will set the target register to nil if no value was removed.

### ByteArraySet

Inserts a value into a byte array.

This instruction requires four arguments:

1. The register to store the written value in, as an integer.
2. The register containing the byte array to write to.
3. The register containing the index to store the byte at.
4. The register containing the integer to store in the byte array.

This instruction will panic if any of the bytes is not in the range 0..256.

Unlike ArraySet, this instruction will panic if the index is out of bounds.

### ByteArrayToString

Converts a byte array to a string.

This instruction requires three arguments:

1. The register to store the result in.
2. The register containing the byte array to convert.
3. The register containing a boolean indicating if the input array should be
drained.

### CopyBlocks

Copies all of the blocks of one object into another object.  Only blocks defined
directly on the source object will be copied.

This instruction requires two arguments:

1. The register containing the object to copy the blocks to.
2. The register containing the object to copy the blocks from.

### DirectoryCreate

Creates a new directory.

This instruction requires three arguments:

1. The register to store the result in, which is always `nil`.
2. The register containing the path to create.
3. A register containing a boolean. When set to `true` the path is created
recursively.

This instruction may throw an IO error as a string.

### DirectoryList

Lists the contents of a directory.

This instruction requirs two arguments:

1. The register to store the result in, as an array of strings.
2. The register containing the path to the directory.

This instruction may throw an IO error as a string.

### DirectoryRemove

Removes an existing directory.

This instruction requires three arguments:

1. The register to store the result in, which is always `nil`.
2. The register containing the path to remove.
3. A register containing a boolean. When set to `true` the contents of the
   directory are removed before removing the directory itself.

This instruction may throw an IO error as a string.

### Drop

Immediately drops the value of an object, if any.

This instruction takes one argument: the register containing the object for
which to drop the value.

If the object has no value this instruction won't do anything.

Once dropped the value of the object should no longer be used as its memory may
have been deallocated.

### Exit

Terminates the VM with a given exit status.

This instruction takes one argument: a register containing an integer to use for
the exit status.

### FileCopy

Copies a file from one location to another.

This instruction requires three arguments:

1. The register to store the number of copied bytes in as an integer.
2. The register containing the file path to copy.
3. The register containing the new path of the file.

### FileFlush

Flushes a file.

This instruction requires one argument: the register containing the file to
flush.

This instruction may throw an IO error as a string.

### FileOpen

Opens a file handle in a particular mode (read-only, write-only, etc).

This instruction requires four arguments:

1. The register to store the file object in.
1. The register containing the prototype to use for the resulting file object.
1. The path to the file to open.
1. The register containing an integer that specifies the file open mode.

The available file modes supported are as follows:

| Value | Mode
|:------|:---------
| `0`   | read-only
| `1`   | write-only
| `2`   | append-only
| `3`   | read+write
| `4`   | read+append

This instruction may throw an IO error as a string.

### FileRead

Reads data from a file into an array of bytes.

This instruction requires three arguments:

1. The register to store the number of read bytes in.
2. The register containing the file to read from.
3. The register containing the byte array to read the data into.
4. The register containing the number of bytes to read. If set to nil, all
remaining data is read.

This instruction may throw an IO error as a string.

### FileRemove

Removes a file.

This instruction takes two arguments:

1. The register to store the result in. This register will be set to nil upon
success.
2. The register containing the path to the file to remove.

This instruction may throw an IO error as a string.

### FileSeek

Sets a file cursor to the given offset in bytes.

This instruction requires three arguments:

1. The register to store the new cursor position in.
2. The register containing the input file.
3. The offset to seek to as an integer. This integer must be greater than 0.

This instruction may throw an IO error as a string.

### FileSize

Returns the size of a file in bytes.

This instruction requires two arguments:

1. The register to store the size of the file in.
2. The register containing the path to the file.

This instruction may throw an IO error as a string.

### FileTime

Gets the creation, modification or access time of a file.

This instruction requires three arguments:

1. The register to store the result in as a float.
2. The register containing the file path of the file.
3. The register containing an integer indicating what kind of timestamp to
   retrieve.

This instruction will throw an error message (as a String) if the file's
metadata could not be retrieved.

This instruction will panic if the timestamp kind is invalid. The following
timestamp kinds are available:

| Value | Meaning
|:------|:-------------------------
| `0`   | The creation time.
| `1`   | The modification time.
| `2`   | The access time.

### FileType

Gets the file type of a path.

This instruction requires two arguments:

1. The register to store the result in as an integer.
2. The register containing the path to check.

This instruction can produce the following values:

1. `0`: the path does not exist.
2. `1`: the path is a file.
3. `2`: the path is a directory.

### FileWrite

Writes a string to a file.

This instruction requires three arguments:

1. The register to store the amount of written bytes in.
2. The register containing the file object to write to.
3. The register containing the string or byte array to write.

This instruction may throw an IO error as a string.

### FloatAdd

Adds two floats

This instruction requires three arguments:

1. The register to store the result in.
2. The register of the receiver.
3. The register of the float to add.

### FloatCeil

Gets the ceiling of a float.

This instruction takes two arguments:

1. The register to store the result in as a float.
2. The register containing the float.

### FloatDiv

Divides two floats

This instruction requires three arguments:

1. The register to store the result in.
2. The register of the receiver.
3. The register of the float to divide with.

### FloatEquals

Checks if two floats are equal.

This instruction requires three arguments:

1. The register to store the result in.
2. The register containing the float to compare.
3. The register containing the float to compare with.

The result of this instruction is either boolean true or false.

### FloatFloor

Gets the floor of a float.

This instruction takes two arguments:

1. The register to store the result in as a float.
2. The register containing the float.

### FloatGreater

Checks if one float is greater than the other.

This instruction requires three arguments:

1. The register to store the result in.
2. The register containing the float to compare.
3. The register containing the float to compare with.

The result of this instruction is either boolean true or false.

### FloatGreaterOrEqual

Checks if one float is greater than or requal to the other.

This instruction requires three arguments:

1. The register to store the result in.
2. The register containing the float to compare.
3. The register containing the float to compare with.

The result of this instruction is either boolean true or false.

### FloatIsInfinite

Sets a register to true if a given float register is an infinite number.

This instruction takes two arguments:

1. The register to store the result in.
2. The register containing the float to check.

### FloatIsNan

Sets a register to true if a given float register is a NaN value.

This instruction takes two arguments:

1. The register to store the result in.
2. The register containing the float to check.

### FloatMod

Gets the modulo of a float

This instruction requires three arguments:

1. The register to store the result in.
2. The register of the receiver.
3. The register of the float argument.

### FloatMul

Multiplies two floats

This instruction requires three arguments:

1. The register to store the result in.
2. The register of the receiver.
3. The register of the float to multiply with.

### FloatRound

Rounds a float to the nearest number.

This instruction takes three arguments:

1. The register to store the result in as a float.
2. The register containing the float.
3. The register containing an integer indicating the number of decimals to round
to.

### FloatSmaller

Checks if one float is smaller than the other.

This instruction requires three arguments:

1. The register to store the result in.
2. The register containing the float to compare.
3. The register containing the float to compare with.

The result of this instruction is either boolean true or false.

### FloatSmallerOrEqual

Checks if one float is smaller than or requal to the other.

This instruction requires three arguments:

1. The register to store the result in.
2. The register containing the float to compare.
3. The register containing the float to compare with.

The result of this instruction is either boolean true or false.

### FloatSub

Subtracts two floats

This instruction requires three arguments:

1. The register to store the result in.
2. The register of the receiver.
3. The register of the float to subtract.

### FloatToInteger

Converts a float to an integer

This instruction requires two arguments:

1. The register to store the result in.
2. The register of the float to convert.

### FloatToString

Converts a float to a string

This instruction requires two arguments:

1. The register to store the result in.
2. The register of the float to convert.

### FloatToBits

Stores the bitwise representation of a float as an integer.

This instruction requires two arguments:

1. The register to store the Integer in.
2. The register containing the float to convert.

### GetAttribute

Gets an attribute from an object and stores it in a register.

This instruction requires three arguments:

1. The register to store the attribute's value in.
2. The register containing the object from which to retrieve the attribute.
3. The register containing the attribute name.

If the attribute does not exist the target register is set to nil.

### GetAttributeNames

Gets all the attributes names available on an object.

This instruction requires two arguments:

1. The register to store the attribute names in.
2. The register containing the object for which to get all attributes names.

### GetFalse

Sets a "false" value in a register.

This instruction requires only one argument: the register to store the object
in.

### GetGlobal

Gets a global variable and stores it in a register.

This instruction requires two arguments:

1. The register to store the global's value in.
2. The global variable index to get the value from.

### GetLocal

Gets a local variable and stores it in a register.

This instruction requires two arguments:

1. The register to store the local's value in.
2. The local variable index to get the value from.

### GetNil

Sets the nil singleton in a register.

This instruction requires only one argument: the register to store the object
in.

### GetParentLocal

Gets a local variable in one of the parent bindings.

This instruction requires three arguments:

1. The register to store the local variable in.
2. The number of parent bindings to traverse in order to find the binding to get
the variable from.
3. The local variable index to get.

### GetPrototype

Gets the prototype of an object.

This instruction requires two arguments:

1. The register to store the prototype in.
2. The register containing the object to get the prototype from.

If no prototype was found, nil is set in the register instead.

### GetToplevel

Sets the top-level object in a register.

This instruction requires one argument: the register to store the object in.

### GetTrue

Sets a "true" value in a register.

This instruction requires only one argument: the register to store the object
in.

### Goto

Jumps to a specific instruction.

This instruction takes one argument: the instruction index to jump to.

### GotoIfFalse

Jumps to an instruction if a register is not set or set to false.

This instruction takes two arguments:

1. The instruction index to jump to if a register is not set.
2. The register to check.

### GotoIfTrue

Jumps to an instruction if a register is set.

This instruction takes two arguments:

1. The instruction index to jump to if a register is set.
2. The register to check.

### HasherToHash

Returns the hash for the values written to a hasher.

This instruction requires two arguments:

1. The register to store the result in as an integer.
2. The register containing the hasher to fetch the result from.

### HasherNew

Creates a new hasher.

This instruction requires four arguments:

1. The register to store the hasher in.
1. The register containing the prototype to use for the hasher.
1. The register containing the first key for the random state, as an integer.
1. The register containing the second key for the random state, as an integer.

### HasherWrite

Hashes an object

This instruction requires three arguments:

1. The register to store the result in, this is the value that is hashed.
2. The register containing the hasher to use.
3. The register containing the object to hash.

The following objects can be hashed:

1. Integers
2. Big integers
3. Floats
4. strings
5. Permanent objects

### HasherReset

Resets the internal state of a hasher.

This instruction takes two arguments:

1. The register to store the result in.
2. The register containing the hasher to reset.

The resulting value is the hasher provided as the second argument.

### IntegerAdd

Adds two integers

This instruction requires three arguments:

1. The register to store the result in.
2. The register of the left-hand side object.
3. The register of the right-hand side object.

### IntegerBitwiseAnd

Performs an integer bitwise AND.

This instruction requires three arguments:

1. The register to store the result in.
2. The register of the integer to operate on.
3. The register of the integer to use as the operand.

### IntegerBitwiseOr

Performs an integer bitwise OR.

This instruction requires three arguments:

1. The register to store the result in.
2. The register of the integer to operate on.
3. The register of the integer to use as the operand.

### IntegerBitwiseXor

Performs an integer bitwise XOR.

This instruction requires three arguments:

1. The register to store the result in.
2. The register of the integer to operate on.
3. The register of the integer to use as the operand.

### IntegerDiv

Divides an integer

This instruction requires three arguments:

1. The register to store the result in.
2. The register of the left-hand side object.
3. The register of the right-hand side object.

### IntegerEquals

Checks if two integers are equal.

This instruction requires three arguments:

1. The register to store the result in.
2. The register containing the integer to compare.
3. The register containing the integer to compare with.

The result of this instruction is either boolean true or false.

### IntegerGreater

Checks if one integer is greater than the other.

This instruction requires three arguments:

1. The register to store the result in.
2. The register containing the integer to compare.
3. The register containing the integer to compare with.

The result of this instruction is either boolean true or false.

### IntegerGreaterOrEqual

Checks if one integer is greater than or requal to the other.

This instruction requires three arguments:

1. The register to store the result in.
2. The register containing the integer to compare.
3. The register containing the integer to compare with.

The result of this instruction is either boolean true or false.

### IntegerMod

Gets the modulo of an integer

This instruction requires three arguments:

1. The register to store the result in.
2. The register of the left-hand side object.
3. The register of the right-hand side object.

### IntegerMul

Multiplies an integer

This instruction requires three arguments:

1. The register to store the result in.
2. The register of the left-hand side object.
3. The register of the right-hand side object.

### IntegerShiftLeft

Shifts an integer to the left.

This instruction requires three arguments:

1. The register to store the result in.
2. The register of the integer to operate on.
3. The register of the integer to use as the operand.

### IntegerShiftRight

Shifts an integer to the right.

This instruction requires three arguments:

1. The register to store the result in.
2. The register of the integer to operate on.
3. The register of the integer to use as the operand.

### IntegerSmaller

Checks if one integer is smaller than the other.

This instruction requires three arguments:

1. The register to store the result in.
2. The register containing the integer to compare.
3. The register containing the integer to compare with.

The result of this instruction is either boolean true or false.

### IntegerSmallerOrEqual

Checks if one integer is smaller than or requal to the other.

This instruction requires three arguments:

1. The register to store the result in.
2. The register containing the integer to compare.
3. The register containing the integer to compare with.

The result of this instruction is either boolean true or false.

### IntegerSub

Subtracts an integer

This instruction requires three arguments:

1. The register to store the result in.
2. The register of the left-hand side object.
3. The register of the right-hand side object.

### IntegerToFloat

Converts an integer to a float

This instruction requires two arguments:

1. The register to store the result in.
2. The register of the integer to convert.

### IntegerToString

Converts an integer to a string

This instruction requires two arguments:

1. The register to store the result in.
2. The register of the integer to convert.

### LoadModule

Loads a bytecode module and executes it.

A module is only executed the first time it is loaded, after that this
instruction acts like a no-op.

This instruction requires two arguments:

1. The register to store the result in. The first time a module is loaded this
will be set to whatever the module returned, after that it will be set to nil.
2. A register containing the file path to the module, as a string.

### LocalExists

Checks if a local variable exists.

This instruction requires two arguments:

1. The register to store the result in (true or false).
2. The local variable index to check.

### ProcessSetBlocking

Marks or unmarks the current process as performing a blocking operation. When
marking a process as blocking, the VM moves it to a blocking thread pool. When
the mark is unset, the VM moves the process back to the primary thread pool.

This instruction takes one argument: a register containing a boolean that
indicates the blocking state of the process.

If the process is already running in the blocking pool, this instruction does
nothing.

### ProcessIdentifier

Stores the unique identifier of a process in a register, as an integer.

This instruction takes one argument: the register to store the identifier in.

### ObjectEquals

Checks if two objects are equal.

Comparing equality is done by simply comparing the addresses of both pointers:
if they're equal then the objects are also considered to be equal.

This instruction takes three arguments:

1. The register to store the result in.
2. The register containing the object to compare.
3. The register containing the object to compare with.

The result of this instruction is either boolean true, or false.

### Panic

Produces a VM panic.

A VM panic will result in a stack trace and error message being displayed, after
which the VM will terminate.

This instruction requires one argument: the register containing the error
message to display.

### Platform

Returns the type of the platform as a string.

This instruction requires one argument: a register to store the resulting
platform name in.

The possible values produced by this instruction are:

* android
* bitrig
* dragonfly
* freebsd
* ios
* linux
* macos
* netbsd
* openbsd
* unix
* unknown
* windows

If the platform can not be determined, "unknown" will be used.

### ProcessCurrent

Stores the currently running process in the given register.

This instruction requires two arguments:

1. The register to store the process in.
1. The register containing the object to use as the prototype for the process.

### ProcessReceiveMessage

Receives a message for the current process.

This instruction takes two arguments:

1. The register to store the received message in.
2. A timeout in seconds (as a float) after which the process will resume, even
   if no message is received. Using a negative or non float value will result in
   a panic.

If no messages are available the current process will be suspended, and the
instruction will be retried the next time the process is executed.

A timeout of 0.0 is interpreted as no timeout, meaning the process will be
suspended until a message is received, regardless of how long this takes.

### ProcessSendMessage

Sends a message to a process.

This instruction takes three arguments:

1. The register to store the message in.
2. The register containing the process to send the message to.
3. The register containing the message (an object) to send to the process.

### ProcessSpawn

Spawns a new process.

This instruction requires three arguments:

1. The register to store the process in.
1. The register containing the prototype to use for the process.
1. The register containing the Block to run in the process.

### ProcessSuspendCurrent

Suspends the current process.

This instruction takes one argument: a register containing the minimum amount of
seconds (as a float) the process should be suspended. Using a negative value or
a non float value will result in a panic.

### ProcessTerminateCurrent

Terminates the current process.

This instruction does not take any arguments.

### RemoveAttribute

Removes a attribute from an object.

This instruction requires three arguments:

1. The register to store the removed attribute in.
2. The register containing the object from which to remove the attribute.
3. The register containing the attribute name.

If the attribute did not exist the target register is set to nil instead.

### Return

Returns the value in the given register.

This instruction takes two arguments:

1. An integer that indicates if we're performing a regular return (0) or a block
return (1).
2. The register containing the value to return. If no value is given nil will be
returned instead.

When performing a block return we'll first unwind the call stack to the scope
that defined the current block.

### RunBlock

Executes a Block object.

This instruction takes the following arguments:

1. The register to store the return value in.
2. The register containing the Block object to run.
3. An integer indicating the number of positional arguments.
4. An integer indicating the number of keyword arguments.
5. A variable list of positional arguments.
6. A variable list of keyword argument and value pairs. The keyword argument
   names must be interned strings.

### SetArray

Sets an array in a register.

This instruction requires at least one argument: the register to store the
resulting array in. Any extra instruction arguments should point to registers
containing objects to store in the array.

### SetAttribute

Sets an attribute of an object.

This instruction requires three arguments:

1. The register to store the written value in
2. The register containing the object for which to set the attribute.
3. The register containing the attribute name.
4. The register containing the object to set as the value.

### SetAttributeToObject

Sets the attribute of an object to an empty object, but only if the attribute is
not already set.

This instruction requires three arguments:

1. The register to store the object set in.
2. The register containing the object to store the attribute in.
3. The register containing the name of the attribute.

### SetBlock

Sets a Block in a register.

This instruction requires two arguments:

1. The register to store the object in.
2. The index of the CompiledCode object literal to use for creating the Block.

If the underlying CompiledCode object captures any outer locals the block's
binding will have its parent set to the binding of the current context.

A block that captures local variables can not be safely stored in a global
object as this can result in the captured locals outliving the process they were
allocated in.

### SetGlobal

Sets a global variable to a given register's value.

This instruction requires three arguments:

1. The register to store the written value in.
2. The global variable index to set.
3. The register containing the object to store in the variable.

If the object being stored is not a permanent object it will be copied to the
permanent generation.

### SetLiteral

Sets a literal value in a register.

This instruction requires two arguments:

1. The register to store the literal value in.
2. The index to the value in the literals table of the current compiled code
object.

### SetLocal

Sets a local variable to a given register's value.

This instruction requires two arguments:

1. The local variable index to set.
2. The register containing the object to store in the variable.

### SetObject

Sets an object in a register.

This instruction takes three arguments:

1. The register to store the object in.
2. A register containing a truthy/falsy object. When the register contains a
truthy object the new object will be a permanent object.
3. An optional register containing the prototype for the object.

### SetParentLocal

Sets a local variable in one of the parent bindings.

This instruction requires three arguments:

1. The local variable index to set.
2. The number of parent bindings to traverse in order to find the binding to set
the variable in.
3. The register containing the value to set.

### SetPrototype

Sets the prototype of an object.

This instruction requires three arguments:

1. The register to store the new prototype in.
1. The register containing the object for which to set the prototype.
2. The register containing the object to use as the prototype.

### SetRegister

Sets a register to the value of another register.

This instruction requires two arguments:

1. The register to set.
2. The register to get the value from.

### Stacktrace

Produces a stack trace.

This instruction requires the three arguments:

1. The register to store the trace in.
2. A register containing the maximum number of frames to include. If set to nil
all frames will be included.
3. A register containing the number of call frames to skip (from the start of
the stack).

The trace is stored as an array of arrays. Each sub array contains:

1. The path of the file being executed.
2. The name of the ExecutionContext.
3. The line of the ExecutionContext.

The frames are returned in reverse order. This means that the most recent call
frame is the last value in the array.

### StderrFlush

Flushes all output to STDERR.

This instruction takes one argument: a register to set to nil if the output was
flushed successfully.

This instruction may throw an IO error as a string.

### StderrWrite

Writes a string to STDERR and returns the amount of written bytes.

This instruction requires two arguments:

1. The register to store the amount of written bytes in.
2. The register containing the string or byte array to write.

This instruction may throw an IO error as a string.

### StdinRead

Reads all the data from STDIN.

This instruction requires two arguments:

1. The register to store the number of read bytes in.
2. The register containing the byte array to read the data into.
3. The register containing the number of bytes to read. If set to nil, all
remaining data is read.

This instruction may throw an IO error as a string.

### StdoutFlush

Flushes all output to STDOUT.

This instruction takes one argument: a register to set to nil if the output was
flushed successfully.

This instruction may throw an IO error as a string.

### StdoutWrite

Writes a string to STDOUT and returns the amount of written bytes.

This instruction requires two arguments:

1. The register to store the amount of written bytes in.
2. The register containing the string or byte array to write.

This instruction may throw an IO error as a string.

### StringConcat

Concatenates two strings together, producing a new one.

This instruction requires three arguments:

1. The register to store the result in.
2. The register containing the first string.
3. The register containing the second string.

### StringConcatMultiple

Takes an array of string objects and concatenates them together efficiently.

This instruction requires two arguments:

1. The register to store the resulting string in.
2. The register containing the array of strings.

### StringEquals

Checks if two strings are equal.

This instruction requires three arguments:

1. The register to store the result in.
2. The register of the string to compare.
3. The register of the string to compare with.

### StringFormatDebug

Formats a string for debugging purposes.

This instruction requires two arguments:

1. The register to store the result in, as a string.
2. The register containing the string to format.

### StringLength

Returns the amount of characters in a string.

This instruction requires two arguments:

1. The register to store the result in.
2. The register of the string.

### StringSize

Returns the amount of bytes in a string.

This instruction requires two arguments:

1. The register to store the result in.
2. The register of the string.

### StringSlice

Slices a string into a new string.

Slicing operates on the _characters_ of a string, not the bytes.

This instruction requires four arguments:

1. The register to store the new string in.
2. The register containing the string to slice.
3. The register containing the start position.
4. The register containing the number of values to include.

### StringToByteArray

Returns a byte array containing the bytes of a given string.

This instruction requires two arguments:

1. The register to store the result in.
2. The register containing the string to get the bytes from.

### StringToLower

Returns the lowercase equivalent of a string.

This instruction requires two arguments:

1. The register to store the new string in.
2. The register containing the input string.

### StringToUpper

Returns the uppercase equivalent of a string.

This instruction requires two arguments:

1. The register to store the new string in.
2. The register containing the input string.

### StringToFloat

Converts a string to a float.

This instructino requires two arguments:

1. The register to store the resulting float in.
2. The register containing the string to convert.

### StringToInteger

Converts a string to an integer.

This instructino requires two arguments:

1. The register to store the resulting integer in.
2. The register containing the string to convert.

### StringByte

Obtains a single byte from a string.

This instruction requires three arguments:

1. The register to store the resulting byte (as an integer) in.
2. The register containing the string to read the byte from.
3. The register containing the (byte) index of the byte to obtain.

### TailCall

Performs a tail call on the current block.

This instruction takes the same arguments as RunBlock, except for the register
and block arguments.

### Throw

Throws a value

This instruction requires one arguments: the register containing the value to
throw.

This method will unwind the call stack until either the value is caught, or
until we reach the top level (at which point we terminate the VM).

### TimeMonotonic

Gets the current value of a monotonic clock in seconds.

This instruction requires one argument: the register to set the time in, as a
float.

### TimeSystem

Gets the current system time.

This instruction takes one argument: the register to store the number of seconds
since the Unix epoch in seconds (including fractional seconds), as a Float.

### TimeSystemDst

Determines if DST is active or not.

This instruction requires one argument: the register to store the result in as a
boolean.

### TimeSystemOffset

Gets the system time's offset to UTC in seconds.

This instruction takes one argument: the register to store the offset in as an
integer.

### EnvGet

Reads the value of an environment variable.

This instruction requires two arguments:

1. The register to store the value in, as a string or nil.
2. The register containing the name of the environment variable, as a string.

The result will be nil if the environment variable is not set.

### EnvSet

Sets the value of an environment variable.

This instruction requires three arguments:

1. The register to store the new value in, as a string.
1. The register containing the name of the environment variable, as a string.
1. The register containing the new value, as a string.

### EnvRemove

Removes an environment variable.

This instruction requires two arguments:

1. The register to store the result in, which is currently always nil.
1. The register containing the name of the environment variable to remove, as a
   string.

### EnvHomeDirectory

Gets the home directory of the current user.

This instruction only requires a single argument: the register to store the home
directory in, as a string.

It's possible the home directory is not set, in which case the result of this
instruction will be nil.

### EnvTempDirectory

Gets the temporary directory of the system.

This instruction only requires a single argument: the register to store the
temporary directory in, as a string.

### EnvGetWorkingDirectory

Gets the current working directory.

This instruction only requires a single argument: the register to store the
working directory in, as a string.

This instruction may throw an IO error message as a string.

### EnvSetWorkingDirectory

Changes the current working directory.

This instruction requires two arguments:

1. The register to store the new working directory in.
1. The register containing the new working directory.

This instruction may throw an IO error message as a string.

### EnvArguments

Gets the command-line arguments passed to the running program.

This instruction only requires a single argument: the register to store the
command-line arguments in, as an array of strings.

### EnvVariables

Stores a list of all the environment variable names, as an array of strings.

This instruction takes a single argument: the register to store the names in.

### BlockGetReceiver

Gets the receiver of a block.

This instruction only requires one argument: the register to store the receiver
in.

### BlockSetReceiver

Sets the receiver of a block.

This instruction requires two arguments:

1. The register to store the new receiver in.
1. The register containing the new receiver.

Most blocks have a receiver by default. One exception are blocks executed when
loading a module. In this case BlockSetReceiver can be used to manually set a
receiver.

### RunBlockWithReceiver

Executes a block with a specific receiver.

This instruction requires at least three arguments:

1. The register to store the return value in.
1. The register containing the block to run.
1. The register containing the receiver.
3. An integer indicating the number of positional arguments.
4. An integer indicating the number of keyword arguments.
5. A variable list of positional arguments.
6. A variable list of keyword argument and value pairs. The keyword argument
   names must be interned strings.

See the RunBlock instruction for more information.

### ProcessSetPanicHandler

Sets a panic handler for the current process.

This instruction requires two arguments:

1. The register to store the new panic handler in.
1. The register containing the new panic handler.

### ProcessAddDeferToCaller

Defers the execution of a block in the scope of the caller.

This instruction requires two arguments:

1. The register to store the deferred block in.
1. The register containing the deferred block to schedule.

### ProcessPinThread

Pins the running process to the current OS thread.

This instruction requires a single argument: the register to store the result
in. The result will be the `true` object if the process was pinned, and the
`false` object if the process was already pinned.

### ProcessUnpinThread

Unpins the running process from the current OS thread.

This instruction requires a single argument: the register to store the result
in. The result is always the `nil` object.

### SetDefaultPanicHandler

Sets a new default panic handler.

This instruction requires two arguments:

1. The register to store the new panic handler in.
1. The register containing the new panic handler.

### GetBuiltinPrototype

Retrieves the prototype for a built-in type.

This instruction requires two arguments:

1. The register to store the prototype in.
1. The register containing an integer that indicates what kind of prototype to
   retrieve.

For the second argument, the following values can be used:

| Value   | Prototype
|:--------|:----------
| 0       | Object
| 1       | Integer
| 2       | Float
| 3       | String
| 4       | Array
| 5       | Block
| 6       | Boolean
| 7       | ReadOnlyFile
| 8       | WriteOnlyFile
| 9       | ReadWriteFile
| 10      | ByteArray
| 11      | Hasher
| 12      | Library
| 13      | Function
| 14      | Pointer
| 15      | Process
| 16      | Socket
| 17      | Unix domain socket

### LibraryOpen

Opens a C library, panicking if the library can not be found.

This instruction requires three arguments:

1. The register to store the resulting library in.
1. The register containing the prototype to use for the library object.
1. The register containing one or more names to use for finding the library.

If no names are given, this instruction will panic. If one or more names are
given, this instruction will try to find a corresponding library for every name,
stopping once it finds a library.

For example, when the names `['libc.so', 'libc.so.6']` are given, this
instruction will first try `'libc.so'`, then `'libc.so.6'` if `'libc.so'` could
not be found.

### FunctionAttach

Loads a C function from a library and prepares it for use.

This instruction requires siz arguments:

1. The register to store the function in.
1. The register containing the prototype to use for the function object.
1. The register containing the Library to load the function from.
1. The register containing the name of the function to load, as a String.
1. The register containing an array that specifies the argument types.
1. The register containing an integer that specifies the return type.

Argument and return types are specified using integers. The following values and
their corresponding C types are available:

| Integer value | C type           | Inko Type
|:--------------|:-----------------|:--------------------------------
| 0             | `void`           | Nil
| 1             | `void *`         | Pointer
| 2             | `double`         | Float
| 3             | `float`          | Float
| 4             | `char`           | Integer
| 5             | `short`          | Integer
| 6             | `int`            | Integer
| 7             | `long`           | Integer
| 8             | `unsigned char`  | Integer
| 9             | `unsigned short` | Integer
| 10            | `unsigned int`   | Integer
| 11            | `unsigned long`  | Integer
| 12            | `*char`          | String
| 13            | `*char`          | ByteArray
| 14            | `size_t`         | Integer

Using an invalid function name will result in a panic.

### FunctionCall

Calls a function with a fixed number of arguments.

This instruction requires four arguments:

1. The register to store the result in.
1. The register containing the Function to call.
1. The register containing the arguments to pass, as an array of objects.
1. The register containing the prototype to use when allocating a pointer object.

Supplying an invalid number of arguments or incorrect argument types will result
in a panic.

### PointerAttach

Loads a variable from a library and stores a pointer to it in a register.

This instruction requires four arguments:

1. The register to store the result in.
1. The register containing the prototype to use for the pointer object.
1. The register containing the Library to load the variable from.
1. The register containing the name of the variable to load, as a String.

Using an invalid variable name will result in a panic.

### PointerRead

Reads the value of a pointer into a specific type.

This instruction requires four arguments:

1. The register to store the result in.
1. The register containing the prototype to use when allocating a pointer
   object.
1. The register containing the Pointer to read from.
1. The register containing an integer that specifies what type of data to read
   the value into. See the `FunctionAttach` instruction for the available
   values.

This instruction will panic if an invalid type identifier is specified. Reading
a value into a different type (e.g. reading a `char*` into an `unsigned long`)
may result in platform specific or undefined behaviour.

### PointerWrite

Writes a value of a particular type to a pointer.

This instruction requires four arguments:

1. The register to store the written value in.
1. The register containing the Pointer to write to.
1. The register containing the type of value to write, as an integer.
1. The register containing the object to write to the pointer.

Writing an object that is not compatible with the type identifier will result in
a panic.

This instruction only supports the types described in the `FunctionAttach`
instruction. Custom objects can not be written to a C pointer.

### PointerFromAddress

Creates a Pointer from an address specified in an Integer.

This instruction requires three arguments:

1. The register to store the Pointer into.
1. The register containing the prototype to use for the pointer object.
1. The register containing the address, as an Integer.

### PointerAddress

Stores the address of a pointer as an integer.

This instruction requires two arguments:

1. The register to store the integer in.
2. The register containing the pointer to obtain the address from.

### ForeignTypeSize

Obtains the size of a C type.

This instruction requires two arguments:

1. The register to store the size in, as an Integer.
1. The register containing the C type identifier, as an Integer.

The use of an invalid type identifier will result in a panic.

### ForeignTypeAlignment

Obtains the alignment of a C type.

This instruction requires two arguments:

1. The register to store the alignment in, as an Integer.
1. The register containing the C type identifier, as an Integer.

The use of an invalid type identifier will result in a panic.

### SocketAccept

Accepts an incoming connection on a socket.

This instruction requires three arguments:

1. The register to store the resulting socket in.
1. The register containing the prototype to use for the socket object.
1. The register containing the socket that is accepting connections.

This instruction may throw an IO error as a string.

### SocketAddress

Obtains the local or peer address of a socket.

This instruction requires three arguments:

1. The register to store the resulting address in.
2. The register containing the socket to get the address from.
3. The register containing an integer that indicates what kind of address to
   receive. This must be `0` for the local address, and `1` for the peer
   address; any other value is invalid.

The resulting address is an array containing two values:

1. The address (an IP, Unix socket path, etc) as a string.
2. The port number as an integer. This will always be `0` for Unix domain
   sockets.

This instruction may throw an IO error as a string.

### SocketBind

Binds a socket to an address.

This instruction requires four arguments:

1. The register to store the result in.
2. The register containing the socket to bind.
3. The register containing the address to bind to, as a string.
4. The register containing the port number to bind to, as an integer.

For Unix domain sockets the value of the port register is ignored, but still
required. It is recommended to always set this to `0` for Unix domain sockets.

The result is always the `nil` object.

This instruction may throw an IO error as a string.

### SocketConnect

Connects a socket to an address.

This instruction requires four arguments:

1. The register to store the result in.
2. The register containing the socket to connect.
3. The register containing the address to connect to, as a string.
4. The register containing the port number to connect to, as an integer.

For Unix domain sockets the value of the port register is ignored, but still
required. It is recommended to always set this to `0` for Unix domain sockets.

This instruction may throw an IO error as a string.

### SocketCreate

Creates a new socket.

This instruction requires three arguments:

1. The register to store the socket in.
1. The register containing the prototype to use for the socket object.
1. The register containing the socket domain, as an integer.
1. The register containing the socket type, as an integer.

The following values can be used for the socket domain argument (any other value
is invalid):

| Socket domain | Value
|:--------------|:------
| AF_INET       | `0`
| AF_INET6      | `1`
| AF_UNIX       | `2`

The following values can be used for the socket type argument (any other value
is invalid):

| Socket type    | Value
|:---------------|:------
| SOCK_STREAM    | `0`
| SOCK_DGRAM     | `1`
| SOCK_SEQPACKET | `2`
| SOCK_RAW       | `3`

This instruction may throw an IO error as a string.

### SocketGetOption

Obtains the value of a socket option.

This instruction requires three arguments:

1. The register to store the option value in.
2. The register containing the socket to get the option from.
3. The register containing an integer indicating what option to retrieve.

The following socket option values are available:

| Socket option       | Value | Return type
|:--------------------|:------|:-----------
| IP_TTL              | `0`   | integer
| IPV6_V6ONLY         | `1`   | boolean
| TCP_NODELAY         | `2`   | boolean
| SO_BROADCAST        | `3`   | boolean
| SO_LINGER           | `4`   | float
| SO_RCVBUF           | `5`   | integer
| SO_SNDBUF           | `6`   | integer
| TCP_KEEPALIVE       | `7`   | float
| IP_MULTICAST_LOOP   | `8`   | boolean
| IPV6_MULTICAST_LOOP | `9`   | boolean
| IP_MULTICAST_TTL    | `10`  | integer
| IPV6_MULTICAST_HOPS | `11`  | integer
| IP_MULTICAST_IF     | `12`  | string (containing an IPv4 address)
| IPV6_MULTICAST_IF   | `13`  | integer
| IPV6_UNICAST_HOPS   | `14`  | integer
| SO_REUSE_ADDRESS    | `15`  | boolean
| SO_REUSE_PORT       | `16`  | boolean

This instruction may throw an IO error as a string.

### SocketSetOption

Sets the value of a socket option.

This instruction requires four arguments:

1. The register to store the option value in.
2. The register containing the socket to set the option for.
3. The register containing an integer indicating what option to set.
4. The register containing the value to set the option to.

The options that can be set are the same available to `SocketGetOption`. The
type of the value register should match the return type of the option in
`SocketGetOption`. For example, to set `IP_MULTICAST_IF` you would have to
provide the value as a string.

This instruction may throw an IO error as a string.

### SocketListen

Marks a socket as listening, with a custom backlog.

This instruction requires three arguments:

1. The register to store the result in.
2. The register containing the socket to mark as listening.
3. The register containing the listen backlog, as an integer.

The result is the listen backlog used for the socket.

This instruction may throw an IO error as a string.

### SocketRead

Reads bytes from a connected socket into a byte array.

This instruction requires four arguments:

1. The register to store the number of bytes read in, as an integer.
2. The register containing the socket to read from.
3. The register containing a byte array to read bytes into.
4. The register containing an integer indicating the number of bytes to read.

Bytes read are _appended to_ the byte array, instead of overwriting its
contents.

This instruction may throw an IO error as a string.

### SocketReceiveFrom

Reads bytes from a socket into a byte array, returning the address the data was
sent from.

This instruction requires four arguments:

1. The register to store the sender's address in, as an array.
2. The register containing the socket to read from.
3. The register containing a byte array to read bytes into.
4. The register containing an integer indicating the number of bytes to read.

The address produced is an array containing the following values:

1. The address (an IP, Unix socket path, etc) as a string.
2. The port number as an integer. This will always be `0` for Unix domain
   sockets.

This instruction may throw an IO error as a string.

### SocketSendTo

Sends the contents of a byte array to the given address.

This instruction requires five arguments:

1. The register to store the number of bytes read in, as an integer.
2. The register containing the socket to use for sending the bytes.
3. The register containing the byte array containing the bytes to send.
4. The register containing the destination address as a string.
5. The register containing the destination port number as an integer.

For Unix domain sockets the port number should be set to `0`.

This instruction may throw an IO error as a string.

### SocketWrite

Writes bytes to a connected socket.

This instruction requires three arguments:

1. The register to store the number of bytes written in, as an integer.
2. The register containing the socket to write to.
3. The register containing the byte array to write to the socket.

This instruction may throw an IO error as a string.

### SocketShutdown

Shuts down a socket for reads, writes, or both.

This instruction requires three arguments:

1. The register to store the result in, which is always the `nil` object.
2. The register containing the socket to shut down.
3. The register containing an integer describing which halves to shut down.

The following values can be used for the third argument:

| Value | Meaning
|:------|:----------------------------
| 0     | Shuts down the reading half
| 1     | Shuts down the writing half
| 2     | Shuts down both the reading and writing halves

### RandomNumber

Generates a random number.

This instruction takes two arguments:

1. The register to store the result in.
2. The type of number to produce.

For the second argument, the following values can be used:

| Value | Meaning
|:------|:----------------------------
| 0     | Generate a random integer
| 1     | Generate a random incremental integer
| 2     | Generate a random float

### RandomRange

Generates a random number in a range.

This instruction takes three arguments:

1. The register to store the result in.
2. The register containing the minimum value, as an integer.
3. The register containing the maximum value, as an integer.

If the minimum or maximum value is a float, the resulting value will also be a
float. Mixing of different types for the minimum and maximum values is not
allowed.

### RandomBytes

Generates a sequence of random bytes.

This instruction takes two arguments:

1. The register to store the resulting byte array in.
1. The register containing an integer describing the number of bytes to
   generate.

[tac]: https://en.wikipedia.org/wiki/Three-address_code
