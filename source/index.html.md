---
title: Inko Programming Language
created_at: 2018-07-09
keywords:
  - inko
  - programming language
  - object oriented
  - concurrent
  - interpreted
description: >
  Concurrent and safe object-oriented programming, without the headaches.
---
