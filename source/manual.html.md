---
title: Introduction
---
<!-- vale off -->

The Inko manual covers many aspects of Inko, such as: the syntax, how to use the
compiler, error handling, and much more. The manual is suitable for both
beginners and seasoned veterans.

We also have an [FAQ](/faq) that provides a list of frequently asked questions
and their answers.

The manual does not include documentation of the standard library, this will be
covered separately once a source code documentation program has been developed.

You can contribute to the manual by submitting a merge request to the [Inko
website repository](https://gitlab.com/inko-lang/website).
